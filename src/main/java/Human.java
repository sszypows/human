/**
 * Created by szypows_local on 29.07.2018.
 */

public abstract class Human {
    protected String name;
    protected String surname;
    protected Integer age;


    protected Human(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public abstract String welcome();

    public String introduce() {
        String result = this.welcome() + " jestem " + this.name + " " + this.surname;
        if (this.age != null) {
            result += " wiek: " + this.age;
        }
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
