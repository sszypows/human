import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by szypows_local on 29.07.2018.
 */
public class Census {
    List<Human> list;

    public Census(String lista) {
        String[] tab = lista.split(";");
        this.list = Arrays.stream(tab)
                .map(x -> {
                    String[] humantab = x.split(",");
                    Human human = new Cultural(humantab[0].trim(), humantab[1].trim(), Integer.parseInt(humantab[2].trim()));
                    return human;
                })
                .collect(Collectors.toList());
    }

    public List<Human> getHumanWithName(String nameToFind) {
        return list.stream()
                .filter(x -> x.name.equals(nameToFind))
                .collect(Collectors.toList());
    }

    public List<Human> getHumanWithSurname(String surnameToFind) {
        return list.stream()
                .filter(x -> x.surname.equals(surnameToFind))
                .collect(Collectors.toList());
    }

    public List<Human> getHumanWithAge(int ageToFind) {
        return list.stream()
                .filter(x -> x.age == ageToFind)
                .collect(Collectors.toList());
    }
    public List<Human> getHumanYoungerThanAge(int ageToFind) {
        return list.stream()
                .filter(x -> x.age < ageToFind)
                .collect(Collectors.toList());
    }
}
