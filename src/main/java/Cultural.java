/**
 * Created by szypows_local on 29.07.2018.
 */
public class Cultural extends Human {

    protected Cultural(String name, String surname, int age) {
        super(name, surname, age);
    }

    public String welcome() {
        return "Uszanowanko";
    }

    public static class Builder {
        private String name;
        private String surname;
        private int age;

        public Builder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public String getName() {
            return name;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public String getSurname() {
            return surname;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public int getAge() {
            return age;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Cultural build() {
            return new Cultural(this.name, this.surname, this.age);
        }
    }

}
