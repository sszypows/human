import lombok.AllArgsConstructor;

/**
 * Created by szypows_local on 29.07.2018.
 */

public class Marionette extends Human{

    private HelloRed hello;


    protected Marionette(String name, String surname, int age, HelloRed helloRed) {
        super(name, surname, age);
        this.hello = helloRed;
    }

    @Override
    public String welcome() {
        return hello.hello();
    }


}
