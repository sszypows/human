import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by szypows_local on 29.07.2018.
 */
public class AngryTest {
Human angry = new Angry.Builder("Zenek","Kowalski").setAge(30).build();

    @Test
    public void welcome() {
        assertEquals("Nie przeszkadzaj mi", angry.welcome());
    }
}