import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by szypows_local on 29.07.2018.
 */
public class MarionetteTest {
    HelloRed helloRed = new HelloRed();

    Marionette marionette = new Marionette("Alf","Grzybek",50,helloRed);

    @Test
    public void welcome() {
        assertEquals("Hello Red", marionette.welcome());
    }
}