import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by szypows_local on 01.08.2018.
 */
public class CensusTest {
    Census census = new Census("Adam, Kowalski, 21 ; Karol, Janiak, 31 ; Anna, Marzanna, 18 ; Kasia, Kowalska, 30");

    @Test
    public void should_return_census_created_by_constructor(){
        assertEquals("[Human{name='Adam', surname='Kowalski', age=21}, Human{name='Karol', surname='Janiak', age=31}, Human{name='Anna', surname='Marzanna', age=18}, Human{name='Kasia', surname='Kowalska', age=30}]",census.list.toString());
    }

    @Test
    public void should_return_list_of_human_with_specific_name(){
        assertEquals("[Human{name='Adam', surname='Kowalski', age=21}]",census.getHumanWithName("Adam").toString());
    }

    @Test
    public void should_return_list_of_human_with_specific_surname(){
        assertEquals("[Human{name='Kasia', surname='Kowalska', age=30}]",census.getHumanWithSurname("Kowalska").toString());
    }

    @Test
    public void should_return_list_of_human_with_specific_age(){
        assertEquals("[Human{name='Adam', surname='Kowalski', age=21}]",census.getHumanWithAge(21).toString());
    }

    @Test
    public void should_return_list_of_human_younger_than_specific_age(){
        assertEquals("[Human{name='Anna', surname='Marzanna', age=18}]",census.getHumanYoungerThanAge(21).toString());
    }
}